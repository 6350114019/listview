
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Example4 extends StatelessWidget {
   Example4({Key? key}) : super(key: key);

  final titles = [
    'water lily',
    'tulip',
    'Anthurium flowers',
    'Orchid Blossoms',
    'rose',
    'Yellow roses',
    'Purple Blossoms',
    'Orange blossom',
    'For get me not'
  ];

  final images = [
    AssetImage('assest/1.jpg'),
    AssetImage('assest/2.jpg'),
    AssetImage('assest/3.jpg'),
    AssetImage('assest/4.jpg'),
    AssetImage('assest/5.jpg'),
    AssetImage('assest/6.jpg'),
    AssetImage('assest/7.jpg'),
    AssetImage('assest/8.jpg'),
    AssetImage('assest/9.jpg')
  ];

  final subtitle = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('listView4'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              ListTile(
                leading: CircleAvatar(
                  backgroundImage: images[index],
                  radius: 30,
                ),
                title: Text(
                  '${titles[index]}',
                  style: TextStyle(fontSize: 18),
                ),
                subtitle: Text(
                  subtitle[index],
                  style: TextStyle(fontSize: 15),
                ),
                trailing: Icon(
                  Icons.notifications_none,
                  size: 25,
                ),
                onTap: (){
                  Fluttertoast.showToast(msg:'${titles[index]}',
                    toastLength: Toast.LENGTH_SHORT,
                  );
                },
              ),
              Divider(
                thickness: 1,
              ),
            ],
          );
        },
      ),
    );
  }
}
